
CREATE EXTENSION "cube" ;
CREATE EXTENSION "earthdistance" ;

CREATE TABLE cities
(
  id       BIGINT NOT NULL PRIMARY KEY ,
  location POINT  NOT NULL ,
  name     TEXT   NOT NULL
);

CREATE TABLE pubs
(
  id       BIGINT NOT NULL PRIMARY KEY ,
  location POINT  NOT NULL ,
  name     TEXT   NOT NULL
);

/*
SELECT 5 nearest pubs to the user location.
Assuming POINT( -1.5 , 53 ) is the location of the user.
 */
SELECT name
FROM pubs
ORDER BY ( POINT( -1.5 , 53 ) <-> pubs.location )
LIMIT 6;

----------------------------------------------------------------------------

/*
SELECT 5 nearest pubs to the user location & the distance in miles.

Assuming POINT( -1.5 , 53 ) is the location of the user.
Needs extensions: Cube & earthdistance.
CREATE EXTENSION "cube" ;
CREATE EXTENSION "earthdistance" ;
 */
SELECT name,
       ( POINT( -1.5 , 53 ) <@> pubs.location )::NUMERIC AS "miles away"
FROM pubs
ORDER BY ( POINT( -1.5 , 53 ) <-> pubs.location )
LIMIT 6;

/*
	If we need to speed up queries we can take the advantage of GiST & SP-GiST indexes that both supports the operator: <->
*/