
-- Alter the table to meet the new requirements.
ALTER TABLE pubs ADD COLUMN reviewer_rating SMALLINT CHECK ( reviewer_rating BETWEEN 0 AND 100 ) DEFAULT 0;

-- Seed the table with "data".
UPDATE pubs SET reviewer_rating = floor( random() * 101 ) ;

/*
 Display the pub name, pub closest city, and rating.
 In order to display in what city the pub currently is (not the nearest one), we would need a polygon type that defines the city coordinates + the point location.
 And we'll need to use an intersection method like the present in the PostGis extension: ST_Intersects
 */
SELECT pubs.name            AS pub,
       nearest_pubs.name    AS city,
       pubs.reviewer_rating AS rating
FROM pubs
       JOIN LATERAL (
    SELECT name FROM cities ORDER BY ( cities.location <-> pubs.location ) LIMIT 1
    ) nearest_pubs ON TRUE
WHERE reviewer_rating >= 90
ORDER BY reviewer_rating DESC;

/*
 Display the pub name, pub closest city, and rating.
 In order to display in what city the pub currently is (not the nearest one), we would need a polygon type that defines the city coordinates + the point location.
 And we'll need to use an intersection method like the present in the PostGis extension: ST_Intersects
 */
WITH
  -- Find the closest city to the user, assuming the user location is POINT( -1.5 , 53 )
  nearest_city AS (
    SELECT name, location FROM cities ORDER BY ( POINT( -1.5 , 53 ) <-> cities.location ) LIMIT 1
  ),
  /* Find the nearest 20? pubs to the city
  Note: (we cannot calculate if a pub (point location) is inside a city (point location) a point have 0 area) */
  nearest_pubs AS (
    SELECT name, reviewer_rating
    FROM pubs
    ORDER BY ( ( SELECT location FROM nearest_city ) <-> pubs.location )
    LIMIT 20
  )
-- Find the best 3 among those 20 pubs.
SELECT name, reviewer_rating AS rating
FROM nearest_pubs
ORDER BY rating DESC
LIMIT 3;
