
#### Restoring the database:
In order to restore the database use the CLI tool: `psql`.    
Usually the `psql` tool is found on the installation path of PostgreSQL under the `bin` folder -> `<pg-installation-path>/bin/psql` 

We'll use that tool to restore the database with the provided dump, that can be found in this repository under the name `Toggl-pubs-dump.sql`

The full command would be like this:
`<pg-installation-path>/bin/psql -h <ip> -p <port> -U <user> -W -q -f "./Toggl-pubs-dump.sql" & echo "Done!"` 

#### Where:
- `-h <ip>` is the host -> `-h localhost`
- `-p <port>` is the port -> `-h 5432`
- `-U <user>` is the username -> `-U postgres`
- `-W` is to prompt the password
- `-q` is for quite (do not echo the results into the CMD)
- `-f <file>` is for the file to be read & executed.
- ` & echo "Done!"` will print `"Done!"` when the operation is complete.

You'll asked for the password (possibly twice) of the user you specified, so input it, then just let it run and you'll know when its done.

When finished, you should have a new database called `Toggl`, all the tables are in the `public` schema.

#### Running the query examples:

Each part of the test have a corresponding file.

In some files you'll have some DDL queries, you don't need to run those becuase you already have restored the database. You may want the DML queries to get the data you're looking for.

- `Part 1 - Initial`      -> `Part1.sql`

- `Part 2 - Ratings`      -> `Part2.sql`

- `Part 3 - Comments`     -> `Part3.sql`

- `Part 4 - User ratings` -> `Part4.sql`